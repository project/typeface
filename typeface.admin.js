if (Drupal.jsEnabled && _typeface_js && _typeface_js.loadFace) {
	$(document).ready(function(){
		var typeface = Drupal.settings.typeface;
		var fontsCount = getLength(typeface.fonts);
		var fonts = '';
		var index = 0;
		
		$.each(typeface.fonts, function() {
			var file = this;
			var url = typeface.fonts_path + '/' + this;
			$.getScript( url, function(data, textStatus){
				for(var face in _typeface_js.faces) {
					if(fonts != '') {
						fonts += ';';
					}
					
					fonts += face + '|' + file;
				}
				
				_typeface_js.faces = {};
				index++;
				
				if(index == fontsCount) {
					 $.ajax({
					   type: "POST",
					   url: typeface.set,
					   data: {typeface_fonts: fonts}
					 });
				}
			});
		});
	});
}

function getLength(object) {
	var length = 0;
	for(var element in object) {length++;}
	return length;
}